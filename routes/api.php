<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('signIn','App\Http\Controllers\LoginController@signIn');

Route::get('cars','App\Http\Controllers\DataController@getCars');
Route::get('riders','App\Http\Controllers\DataController@getRiders');
Route::get('directions','App\Http\Controllers\DataController@getDirections');
Route::get('realizations','App\Http\Controllers\DataController@getRealizations');

Route::get('test','App\Http\Controllers\DataController@getTestData');
