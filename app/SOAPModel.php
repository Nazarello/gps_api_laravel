<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \SoapClient;

class SOAPModel extends Model
{

    public function testMethod()
    {
        return ('test data');
    }
    
    private function transform($serverResponse)
    {
        return json_decode($serverResponse->return,true)[0];
    }
    
    private function getSOAPClient()
    {
        $client = new SoapClient("http://192.168.5.37/1c/WS/Westrides.1cws?wsdl", array('login'          => "wsuser",
                                                                                        'password'       => "userws"));
        return $client;
    }

    public function getRiders($userGUID)
    {
        $variable = $this->getSOAPClient()->GetRiders(array('User' => 'df4e881c-70d1-11e6-a247-000c2901aac3'));
        return json_decode($variable->return);
    }

    public function getCars($userGUID)
    {
        $variable = $this->getSOAPClient()->GetCars (array('User' => 'df4e881c-70d1-11e6-a247-000c2901aac3'));
        return json_decode($variable->return);
    }

    public function getDirections($userGUID)
    {
        $variable = $this->getSOAPClient()->GetDirection (array('User' => 'df4e881c-70d1-11e6-a247-000c2901aac3'));
        return json_decode($variable->return);
    }

    public function getDocRealization($direction,$dateStart,$dateEnd,$typeOfRealization,$userGUID)
    {
        $variable = $this->getSOAPClient()->GetDocRealization(array('Direction' => $direction,
                                                                    'DateStart' => $dateStart,
                                                                    'DateEnd' => $dateEnd,
                                                                    'TypeOfRealization' => $typeOfRealization,
                                                                    'User' => 'df4e881c-70d1-11e6-a247-000c2901aac3'));

        return json_decode($variable->return);
    }

    public function GetUsersRules($login,$password)
    {
        $user = $this->getSOAPClient()->GetUsersRules(array('UserName' => "Процик Роман Андрійович", 'UserPassword' => "111"));
        return $this->transform($user);
    }

    
    
    


}
