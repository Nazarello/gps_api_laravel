<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() === "OPTIONS") {

            return response('')
                ->header('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE,OPTIONS')
                ->header('Access-Control-Allow-Origin', 'http://localhost:4200')
                ->header('Access-Control-Allow-Headers', 'access-control-allow-headers,content-type,X-CSRF-TOKEN,X-XSRF-TOKEN');
        }
        else {
            return $next($request)
                ->header('Access-Control-Allow-Origin', 'http://localhost:4200')
                ->header('Access-Control-Allow-Credentials', 'true')
                ->header('Access-Control-Allow-Headers', 'access-control-allow-headers,X-CSRF-TOKEN,X-XSRF-TOKEN');
        }
    }
}
