<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SOAPModel as SOAP;

class DataController extends Controller
{

    public function getCars(Request $request,SOAP $SOAPModel)
    {
        $data = $SOAPModel->getCars('lol');

        return response(json_encode($data), 200);
    }

    public function getRiders(Request $request,SOAP $SOAPModel)
    {
        $data = $SOAPModel->getRiders('lol');

        return response(json_encode($data), 200);
    }

    public function getDirections(Request $request,SOAP $SOAPModel)
    {
        $data = $SOAPModel->getDirections('lol');
        
        return response(json_encode($data), 200);
    }

    public function getTestData(Request $request,SOAP $SOAPModel)
    {
        $data = $SOAPModel->testMethod();
        return response($data, 200);
    }

    public function getRealizations(Request $request,SOAP $SOAPModel)
    {
        $direction = $request->input('Direction');
        $dateStart = $request->input('DateStart');
        $dateEnd = $request->input('DateEnd');
        $typeOfRealization = $request->input('TypeOfRealization');
        $userGUID = 'lol';
        $data = $SOAPModel->getDocRealization($direction,$dateStart,$dateEnd,$typeOfRealization,$userGUID);

        return response($data, 200);
    }


}
