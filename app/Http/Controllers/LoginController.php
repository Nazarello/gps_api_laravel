<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SOAPModel as SOAP;

class LoginController extends Controller
{

    public function signIn(Request $request,SOAP $SOAPModel)
    {
        $data = $SOAPModel->GetUsersRules('lol','kek');

        return response(json_encode($data), 200);
    }
}
